const uuid = require("uuid/v4");
const log = require("./log");
const fakers = require("./fakers")({uuid});
const userRole = require("./user-role")({uuid, log});
const reviewBook = require("./review-book")({uuid, log});
const authorBook = require("./author-book")({uuid, log});
const favoriteBook = require("./favorite-book")({uuid, log});
const follow = require("./follow")({uuid, log});
const publishBook = require("./publish-book")({uuid, log});
const bookCategory = require("./book-category")({uuid, log});
const nodes = require("./nodes")({uuid, fakers});
const {getAt, filterMetaData, dbNodes} = require("./promise");
const gun = require("gun");
require("gun/lib/then");
const db = gun();
const q = require("./query");
const {count, max} = require("./insight");

/* Generate fake data */
// ----------------------
const {
  userNodes,
  roleNodes,
  bookNodes,
  categoryNodes,
  readerNodes,
  authorNodes,
  publisherNodes,
} = nodes.create({db});

const fakeRoles = fakers.fakeRoles();

/* Create user types and roles */
// -----------------
userRole.createNodes({db, userNodes, fakeRoles, readerNodes, authorNodes, publisherNodes});
const roleQs = userRole.queries({db, userNodes, fakeRoles, readerNodes, authorNodes, publisherNodes});
const roleQsPromise = userRole.queriesPromised({db, userNodes, fakeRoles, readerNodes, authorNodes, publisherNodes});
// roleQs["1"]();

/* Readers review books */
// ----------------------
reviewBook.createNodes({db, readerNodes, bookNodes});
const reviewQs = reviewBook.queries({db, readerNodes, bookNodes});
const reviewQsPromise = reviewBook.queriesPromised({db, readerNodes, bookNodes});
// reviewQs["5"]();

/* Authors author books */
// ----------------------
authorBook.createNodes({db, authorNodes, bookNodes});
const authorBookQs = authorBook.queries({db, authorNodes, bookNodes});
const authorBookQsPromise = authorBook.queriesPromised({db, authorNodes, bookNodes});
// authorBookQs["3"]();

/* User has lists of favorite books */
// ----------------------------
favoriteBook.createNodes({db, readerNodes, bookNodes});
const favoriteBookQs = favoriteBook.queries({db, readerNodes, bookNodes});
const favoriteBookQsPromise = favoriteBook.queriesPromised({db, readerNodes, bookNodes});
// favoriteBookQs["3"]();

/* Publisher publishes books */
// ----------------------
publishBook.createNodes({db, publisherNodes, bookNodes});
const publishBookQs = publishBook.queries({db, publisherNodes, bookNodes});
const publishBookQsPromise = publishBook.queriesPromised({db, publisherNodes, bookNodes});
// publishBookQs["4"]();

/* Reader/authors follow other readers/authors */
// ---------------------------------------------
follow.createNodes({db, readerNodes, authorNodes});
const followQs = follow.queries({db, readerNodes, authorNodes});
const followQsPromise = follow.queriesPromised({db, readerNodes, authorNodes});
// followQs["2"]();


/* Books belong to categories */
// ----------------------------
bookCategory.createNodes({db, categoryNodes, bookNodes});
const bookCategoryQs = bookCategory.queries({db, categoryNodes, bookNodes});
const bookCategoryQsPromise = bookCategory.queriesPromised({db, categoryNodes, bookNodes});
// bookCategoryQs["2"]();


/* Holistic Insight */
/* A popular books is a book that has been included in many favorite lists
by readers and is also included in the five star ratings */
Promise.all([
  q(db).get("reviews/5").getSet().get("book").get("uuid").data(),
  q(db).get("readers").getSet().get("favorite_books").getSet().get("books").getSet().get("uuid").data(),
])
.then(qs => {
  const fiveStarBooks = count(qs[0]);
  const topFavorite = max(count(qs[1]), "count");
  let mostPopularBook = {};

  for (const book of fiveStarBooks) {
    if(book.key === topFavorite.key) {
      mostPopularBook = book;
    }
  }

  return q(db).get(mostPopularBook.key).data();
})
.then(mostPopularBook => {
  log("Most popular book");
  log(mostPopularBook);
});
