module.exports = function(deps) {
  const {uuid, log} = deps;

  function authorBook(opt) {
    const {db, author, book, date} = opt;
    const linkId = uuid();

    const authorBookNode = db.get(linkId).put({
      uuid: linkId,
      type: "Link",
      name: "author_book",
      date: date,
    });
    authorBookNode.get("book").put(book);
    authorBookNode.get("author").put(author);

    book.get("authors").set(authorBookNode);
    author.get("books").set(authorBookNode);

    return authorBookNode;
  }

  function createAuthorBookNodes(opt) {
    const {db, authorNodes, bookNodes} = opt;

    /* Author 1 authors books */
    authorBook({
      db,
      author: authorNodes[0],
      book: bookNodes[0],
      date: new Date().toISOString(),
    });

    authorBook({
      db,
      author: authorNodes[0],
      book: bookNodes[1],
      date: new Date().toISOString(),
    });

    /* Reader 2 authors books */
    authorBook({
      db,
      author: authorNodes[1],
      book: bookNodes[0], // author 1 and author 2 co-author book0
      date: new Date().toISOString(),
    });

    authorBook({
      db,
      author: authorNodes[1],
      book: bookNodes[3],
      date: new Date().toISOString(),
    });

    /* Author 3 authors a book */
    authorBook({
      db,
      author: authorNodes[2],
      book: bookNodes[4],
      date: new Date().toISOString(),
    });
  }

  function authorBookQueries(opt) {
    const {db, authorNodes, bookNodes} = opt;
    const queries = {

    1: () => {
      /* Log the titles of the books authored by author 1 */
      authorNodes[0].get("books").map().get("book").get("title").once(log);
    },

    2: () => {
      /* Log the author names of book 1 */
      bookNodes[0].get("authors").map().get("author").get("name").once(log);
    },

    3: () => {
      /*
      Given an author, log the titles of the book they have authored, and any
      rating associated with the books
      */
      authorNodes[0].get("books").map().get("book").get("reviews").map().get("rating").once(log);
    },

    };

    return queries;
  }

  function authorBookQueriesPromised(opt) {
    const {db, authorNodes, bookNodes} = opt;

    const filterMetadata = (o) => {
      const copy = {...o};
      delete copy._;
      return copy;
    };

    const queries = {
      1: () => {

      },

    };

    return queries;
  }

  return {
    createNodes: createAuthorBookNodes,
    queries: authorBookQueries,
    queriesPromised: authorBookQueriesPromised,
  }
};

