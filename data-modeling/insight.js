const count = (arr) => {
  const map = {};

  for(const v of arr) {
    if(v in map) {
      map[v] += 1;
    } else {
      map[v] = 1;
    }
  }

  const r = [];
  Object.keys(map).forEach(k => {
    r.push({
      key: k,
      count: map[k],
    })
  });

  return r;
};

const max = (arr, k) => {
  return arr.slice().sort((a, b) => b[k] - a[k])[0];
};

module.exports = {
  count,
  max,
};
