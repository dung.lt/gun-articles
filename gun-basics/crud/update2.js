const n1 = db.get('5416')
  .put({
    name: 'n1',
    prop: '...',
    doc1: {
      prop: '...',
    },
  });

const n2 = db.get('8899')
  .put({
    name: 'n2',
    doc2: {
      prop: '...',
    }
  });

n1.get('related_to').put(n2);

/*
```
n1.get('doc1').put({
  prop: 'other value'
});
```

- Update what `n1` is related to

```
n1.get('related_to').put(db.get('9185').put({ new_prop: 'some value', }));
```

In the snippet above we completely change what `n1` is pointing to by creating a
new node. Note that `n2` didn't change, we just updated the `related_to`
pointer.

- Add new properties to `n2`, first by referencing from `n1`:

```
n1.get('related_to').put({
  new_stuff: 'some value',
  other_stuff: 'some value',
})
```
 */
