const node1 = db.get('876610').put({
  uuid: '876610',
  name: 'node 1',
});

const node2 = db.get('9909871').put({
  uuid: '9909871',
  name: 'node 2',
});

node1.get('related_to').put({
  property: "value",
  property2: "value",
});

node1.get('related_to').get('node').put(node2);
